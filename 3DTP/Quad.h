#pragma once
#include "entity.h"

class Quad :
	public Entity
{
public:
	Quad(void);
	Quad(float offsetX, float offsetY, float width, float height);

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );
	virtual void LoadContent( LPDIRECT3DDEVICE9 device,  float offsetX, float offsetY, float width, float height  );

	virtual void Draw( Effect *effect, ICamera *cam );

};

