#include "StdAfx.h"
#include "Sky.h"

Sky::Sky()
{
	D3DXMatrixTranslation(&_translationMatrix, 0, 0, 0);
	D3DXMatrixRotationYawPitchRoll(&_rotationMatrix, 0, 0, 0);
	SetScale(D3DXVECTOR3(1,1,1));

	_offset = D3DXVECTOR4(0, 0, 0, 0);
}

Sky::~Sky()
{
}

void Sky::LoadContent(LPDIRECT3DDEVICE9 device)
{
	_device = device;

	LPD3DXBUFFER pD3DXMtrlBuffer;

	// Load the mesh from the specified file
	if (FAILED(D3DXLoadMeshFromX( L"dome.x", D3DXMESH_SYSTEMMEM,
                                   device, NULL,
                                   &pD3DXMtrlBuffer, NULL, &_dwNumMaterials,
                                   &_pMesh)))
		DebugBreak();

	if(FAILED(D3DXCreateTextureFromFile(device, L"Textures\\clouds_big_2.jpg", &_skyTexture)))
		DebugBreak();

	if(FAILED(D3DXCreateTextureFromFile(device, L"Textures\\clouds_big.png", &_skyTexture2)))
		DebugBreak();
}

void Sky::Update()
{
	_offset.x += 0.0001f;
	_offset.y += 0.0001;
}

void Sky::Draw(Effect* effect, ICamera *cam)
{
	D3DXMATRIX world = _scaleMatrix * _rotationMatrix * _translationMatrix;
	D3DXMATRIX WorldViewProj = world;

	WorldViewProj = world * cam->View() * cam->Projection();
	effect->setParameter("world", &world);
	effect->setParameter("worldViewProj", &WorldViewProj);
	effect->setParameter("tex", _skyTexture);
	effect->setParameter("tex2", _skyTexture2);
	effect->setParameter("skyMoveOffset", &_offset);

	effect->setTechnique("Sky");
	
	ID3DXEffect *e = effect->getEffect();

	for( UINT iSubset = 0; iSubset < _dwNumMaterials; iSubset++ )
    {
		HRESULT hr;
		UINT iPass, cPasses;

		e->Begin(&cPasses, 0);

		for( iPass = 0; iPass < cPasses; iPass++ )
		{
			e->BeginPass(iPass);
			_pMesh->DrawSubset(iSubset);
			e->EndPass();
		}
		e->End();
    }

	effect->setTechnique("Default");
}

Sky* Sky::SetPosition( D3DXVECTOR3 translation )
{
	D3DXMatrixTranslation(&_translationMatrix, translation.x, translation.y, translation.z);
	return this;
}

Sky* Sky::SetScale( D3DXVECTOR3 scale )
{
	D3DXMatrixScaling(&_scaleMatrix, scale.x, scale.y, scale.z);
	return this;
}