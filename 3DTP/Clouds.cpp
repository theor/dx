#include "StdAfx.h"
#include "Clouds.h"


Clouds::Clouds(void)
{
	_delay = 0;

	srand(2000000000);
}


Clouds::~Clouds(void)
{
	// Delete clouds
	for (std::list<Cloud*>::iterator it = _clouds.begin(); it != _clouds.end(); ++it)
	{
		delete (*it);
	}
}

void Clouds::LoadContent(LPDIRECT3DDEVICE9 device)
{
	_device = device;
}

void Clouds::Update()
{
	return;

	// Update clouds
	std::list<Cloud*>::iterator it = _clouds.begin();
	while (it != _clouds.end())
	{
		(*it)->Update();

		if (!(*it)->IsAlive())
		{
			_clouds.erase(it++);
		}
		else
			++it;
	}

	--_delay;

	// Wait
	if (_delay < 0)
	{
		_delay = 200000;

		D3DXVECTOR3 pos = D3DXVECTOR3(-10, 160, -10);

		// Clouds creation
		for (int i = 0; i < 200; i++)
		{
			D3DXVECTOR3 offset = D3DXVECTOR3(GetRandom(-2000, 20), GetRandom(0, 300), GetRandom(-2000, 20));
			D3DXVECTOR3 myPos = pos + offset;

			/*for (int j = 0; j < 5; j++)
			{*/
				AddCloud(myPos + D3DXVECTOR3(GetRandom(-20, 20), GetRandom(-20, 20), GetRandom(-20, 20)), 
						 D3DXVECTOR3(GetRandom(-10, 10) / 1000, GetRandom(-10, 10) / 1000, GetRandom(-10, 10) / 1000), 
						 GetRandom(100, 1000));
				//AddCloud(myPos + D3DXVECTOR3(GetRandom(-20, 20), GetRandom(-20, 20), GetRandom(-20, 20)), D3DXVECTOR2(0.01f, 0.01f), 200);
				//AddCloud(myPos + D3DXVECTOR3(GetRandom(-20, 20), GetRandom(-20, 20), GetRandom(-20, 20)), D3DXVECTOR2(0.01f, 0.01f), 200);
			//}
		}
	}
}

void
Clouds::AddCloud(D3DXVECTOR3 pos, D3DXVECTOR3 velocity, float lifeTime)
{
	Cloud* c = new Cloud(lifeTime, pos, velocity);
	c->LoadContent(_device);
	_clouds.push_back(c);
}

float
Clouds::GetRandom(float min, float max)
{
	float delta = max - min;
	float random = rand() % (int)delta;

	return random - delta / 2;
}

void Clouds::Draw(Effect* effect, ICamera *cam)
{
	Functor functor(cam);
	_clouds.sort(functor);
	
	// Update clouds
	for (std::list<Cloud*>::iterator it = _clouds.begin(); it != _clouds.end(); ++it)
	{
		(*it)->Draw(effect, cam);
	}
}

