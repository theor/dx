#pragma once
#include "IEntity.h"
class ICamera
	: public IEntity
{
public:

	virtual D3DXMATRIX View(){return _view;}
	virtual D3DXMATRIX Projection(){return _projection;}

	virtual void LoadContent(LPDIRECT3DDEVICE9 device){};
	virtual void Update() = 0;

	D3DXVECTOR3 Position() const { return _position; }
	void Position(D3DXVECTOR3 val) { _position = val; }

protected:
	D3DXMATRIX _view;
	D3DXMATRIX _projection;
	D3DXVECTOR3 _position;
};

