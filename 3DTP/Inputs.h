#pragma once
class Inputs;

class Inputs
{
public:
	static bool IsPressed(WPARAM keyCode);
	static bool IsPressedOnce(WPARAM keyCode);
	void Update(UINT message, WPARAM keyCode );
	static Inputs* Get();
private:
	Inputs(void);
	~Inputs(void);
	bool _keys[0xFE];
	bool _oldKeys[0xFE];

};

