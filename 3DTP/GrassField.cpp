#include "StdAfx.h"
#include "GrassField.h"


GrassField::GrassField(float side, float* height, int step)
	: _grasses()
{
	for (int i = 0; i < side / step; i++)
	{
		Grass* g = new Grass(side, height, i, step, D3DXVECTOR3(1,0,0));
		_grasses.push_back(g);
	}
}


GrassField::~GrassField(void)
{
}

void GrassField::Draw( Effect *effect, ICamera *cam )
{
	Functor functor(cam);
	_grasses.sort(functor);
	for(GrassList::const_iterator it = _grasses.begin(); it != _grasses.end(); ++it)
	{
		(*it)->Draw(effect, cam);
	}
}

void GrassField::LoadContent( LPDIRECT3DDEVICE9 device )
{
	for(GrassList::const_iterator it = _grasses.begin(); it != _grasses.end(); ++it)
		(*it)->LoadContent(device);
}

void GrassField::Update()
{
	for(GrassList::const_iterator it = _grasses.begin(); it != _grasses.end(); ++it)
		(*it)->Update();
}
