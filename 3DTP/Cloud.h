#pragma once

#include "Entity.h"
#include "IDrawableEntity.h"
#include "Camera.h"

class Cloud : public IDrawableEntity
{
public:
	Cloud(float lifeTime, D3DXVECTOR3 position, D3DXVECTOR3 velocity);
	~Cloud(void);

	Cloud* SetPosition(D3DXVECTOR3 translation);
	Cloud* SetRotation(float rotation);
	Cloud* SetScale(D3DXVECTOR3 scale);
	D3DXMATRIX GetPosition(){return _translationMatrix;}
	D3DXVECTOR3 GetPositionV(){return _position;}

	bool IsAlive() const;

	virtual void Draw( Effect *effect, ICamera *cam );
	virtual void LoadContent( LPDIRECT3DDEVICE9 device );
	virtual void Update();

private:
	virtual void SetVertices(Vertex vertices[], int nb);
	virtual void SetIndices(WORD indexes[], int nb);

	IDirect3DTexture9* _cloudsTexture;

	D3DXMATRIX _translationMatrix;
	D3DXMATRIX _rotationMatrix;
	D3DXMATRIX _scaleMatrix;

	D3DXVECTOR3 _position;
	float _rotation;

	IDirect3DVertexBuffer9*  _vertices;
	IDirect3DIndexBuffer9*  _indexes;
	LPDIRECT3DDEVICE9 _device;
	int _nbVertices;
	int _nbIndexes;
	int _nbPrims;
	float _scale;

	std::string _filename;

	// Animation
	float _lifeTime;
	float _startLifeTime;
	float _alpha;

	D3DXVECTOR3 _velocity;
};