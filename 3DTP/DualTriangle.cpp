#include "StdAfx.h"
#include "DualTriangle.h"


DualTriangle::DualTriangle(void)
{
}


DualTriangle::~DualTriangle(void)
{
}

void DualTriangle::LoadContent( LPDIRECT3DDEVICE9 device )
{
	__super::LoadContent(device);

	/*Vertex v[] = {
		{D3DXVECTOR3(-1, 1,10), D3DCOLOR_COLORVALUE(1,0,0,1)},
		{D3DXVECTOR3( 1, 1,10), D3DCOLOR_COLORVALUE(0,0,1,1)},
		{D3DXVECTOR3(-1,-1,10), D3DCOLOR_COLORVALUE(0,1,0,1)},
		{D3DXVECTOR3( 1,-1,10), D3DCOLOR_COLORVALUE(1,1,1,1)},
	};

	SetVertices(v, sizeof(v));*/

	WORD i[] ={	0, 1, 2	};
	WORD i2[] = {2, 1, 3};

	SetIndices(i, 3);
	SetIndices(&_indexes2, i2, 3);

	_nbPrims = 1;
}

void DualTriangle::Draw( ID3DXEffect *effect, ICamera *cam )
{
	_device->SetStreamSource(0, _vertices, 0, sizeof(Vertex));

	_device->SetIndices(_indexes2);

	effect->SetTechnique("Default");

	D3DXMATRIX world = _translationMatrix;// * _rotationMatrix;
	D3DXMATRIX WorldViewProj= world * cam->View() * cam->Projection();//_view * _projection;
	D3DXHANDLE worldViewProjParam = effect->GetParameterByName(NULL, "worldViewProj");
	effect->SetMatrix(worldViewProjParam , &WorldViewProj);

	unsigned int cPasses, iPass;
	effect->Begin(&cPasses, 0);
	for (iPass= 0; iPass< cPasses; ++iPass)
	{
		effect->BeginPass(iPass);
		effect->CommitChanges();

		_device->SetIndices(_indexes2);
		_device->DrawIndexedPrimitive(_primType, 0, 0, _nbVertices, 0, _nbPrims);
		_device->SetIndices(_indexes);
		_device->DrawIndexedPrimitive(_primType, 0, 0, _nbVertices, 0, _nbPrims);
		effect->EndPass();
	}
	effect->End();
}
