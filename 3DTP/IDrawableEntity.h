#pragma once
#include "ICamera.h"
#include "IEntity.h"
#include "Effect.h"

class IDrawableEntity
	: public IEntity
{
public:
	virtual void Draw(Effect *effect, ICamera *cam) = 0;
};

