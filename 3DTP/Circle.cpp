#include "StdAfx.h"
#include "Circle.h"
#include <cmath>

Circle::Circle(void)
{
}

void Circle::LoadContent( LPDIRECT3DDEVICE9 device )
{
	__super::LoadContent(device);

	const int nb  = 40;

	Vertex v[nb];
	v[0].position = D3DXVECTOR3(0,0,10);
//	v[0].color = D3DCOLOR_COLORVALUE(1,0,0,1);
	for (int i = 1; i < nb; i++)
	{
	//	v[i].color = D3DCOLOR_COLORVALUE(0,i * 20, 0, 1);
		v[i].position = D3DXVECTOR3(-2 * cos(i * 6.28 / (nb - 2)), 2 * sin(i * 6.28 / (nb - 2)), 10);
	}

	SetVertices(v, nb);
	_nbPrims = (nb - 2);
	_primType = D3DPT_TRIANGLEFAN;
}
