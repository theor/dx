#pragma once
#include "Entity.h"
#include "IDrawableEntity.h"
#include <list>
#include "Grass.h"
#include "Camera.h"
class GrassField : public IDrawableEntity, public IEntity
{
public:
	typedef std::list<Grass*> GrassList;

	GrassField(float side, float* height, int step);
	~GrassField(void);

	virtual void Draw( Effect *effect, ICamera *cam );

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );

	virtual void Update();
private:
	GrassList _grasses;
	struct Functor
	{
		Functor(ICamera* c):camera(c){}
		bool operator()( Grass* a, Grass* b ) 
		{
			D3DXMATRIX ma = a->GetPosition();
			D3DXMATRIX mb = b->GetPosition();
			D3DXVECTOR3 va(ma._41, ma._42, ma._43);
			D3DXVECTOR3 vb(mb._41, mb._42, mb._43);
			float res = (D3DXVec3LengthSq(&(camera->Position() - va)) - D3DXVec3LengthSq(&(camera->Position() - vb)));
			return res < 0;
		}
		ICamera* camera;
	};
};

