#include "StdAfx.h"
#include "OrthoCamera.h"
#include "Inputs.h"


OrthoCamera::OrthoCamera(void)
{
	_position = D3DXVECTOR3(0,0,0);
	D3DXMatrixOrthoLH(&_projection, 2, 2, -1, 100);
	D3DXMatrixLookAtLH(&_view, &_position, &D3DXVECTOR3(0,0,1), &D3DXVECTOR3(0,1,0));
}


OrthoCamera::~OrthoCamera(void)
{
}

void OrthoCamera::Update()
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0,0,1);
	if(Inputs::IsPressed(VK_UP))
		_position += dir;
	if(Inputs::IsPressed(VK_DOWN))
		_position -= dir;
	D3DXMatrixLookAtLH(&_view, &_position, &D3DXVECTOR3(0,0,1), &D3DXVECTOR3(0,1,0));
}
