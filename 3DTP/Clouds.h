#pragma once

#include "Entity.h"
#include "IDrawableEntity.h"
#include "Camera.h"
#include "Cloud.h"

#include <list>

class Clouds : public IDrawableEntity
{
public:
	Clouds(void);
	virtual ~Clouds(void);

	virtual void Draw( Effect *effect, ICamera *cam );
	virtual void LoadContent( LPDIRECT3DDEVICE9 device );
	virtual void Update();

private:

	// Helper methodes
	void AddCloud(D3DXVECTOR3 pos, D3DXVECTOR3 velocity, float lifeTime);
	float GetRandom(float min, float max);

	LPDIRECT3DDEVICE9 _device;
	std::list<Cloud*> _clouds;

	float _delay;


	struct Functor
	{
		Functor(ICamera* c):camera(c){}
		bool operator()( Cloud* a, Cloud* b ) 
		{
			D3DXVECTOR3 va = a->GetPositionV();
			D3DXVECTOR3 vb = b->GetPositionV();
			float res = (D3DXVec3LengthSq(&(camera->Position() - va)) - D3DXVec3LengthSq(&(camera->Position() - vb)));
			return res > 0;
		}
		ICamera* camera;
	};
};

