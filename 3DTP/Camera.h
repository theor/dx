#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include "IEntity.h"
#include "ICamera.h"

class Camera
	: public ICamera
{
public:
	Camera(void);
	~Camera(void);

	D3DXMATRIX virtual View(){return _view;}
	D3DXMATRIX virtual Projection(){return _projection;}

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );

	virtual void Update();

	void SetPosition(D3DXVECTOR3 pos){_position = pos;}
	void SetDirection(D3DXVECTOR3 dir){D3DXVec3Normalize(&_lookDir, &dir); _lookat = _position + _lookDir;}

	D3DXVECTOR3 _lookDir;
	D3DXVECTOR3 _lookat;
	D3DXVECTOR3 _up;
};

