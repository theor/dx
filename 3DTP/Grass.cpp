#include "StdAfx.h"
#include "Grass.h"


Grass::Grass(float side, float* height, int offset,int step, D3DXVECTOR3 n)
	: _side(side),
	_height(height),
	_offset(offset),
	_normal(n),
	_step(step)
{
	_rnd = rand()/(float)RAND_MAX;
}


Grass::~Grass(void)
{
}

void setBlade(int offset, D3DXVECTOR3 center,  D3DXVECTOR3 center2, D3DXVECTOR3 normal, Vertex *v, WORD *i)
{
	D3DXVECTOR3 tan;
	D3DXVec3Cross(&tan, &normal, &D3DXVECTOR3(0,1,0));
	//tan *= -8;
	D3DXVECTOR3 bitan = D3DXVECTOR3(0,5,0);

	int off = offset*4;

	v[off].normal = normal;
	v[off].uv = D3DXVECTOR2(0, 0);
	v[off].position = center + bitan;
	  
	v[off+1].normal = normal;
	v[off+1].uv = D3DXVECTOR2(1, 0);
	v[off+1].position = center2 + bitan;
	  
	v[off+2].normal = normal;
	v[off+2].uv = D3DXVECTOR2(0, 1);
	v[off+2].position = center;
	  
	v[off+3].normal = normal;
	v[off+3].uv = D3DXVECTOR2(1, 1);
	v[off+3].position = center2;

	i[offset*6] = off;
	i[offset*6+1] = off+1;
	i[offset*6+2] = off+2;
	i[offset*6+3] = off+1;
	i[offset*6+4] = off+2;
	i[offset*6+5] = off+3;

}

void Grass::LoadContent( LPDIRECT3DDEVICE9 device )
{
	__super::LoadContent(device);

	D3DXMATRIX rot;
	D3DXMatrixRotationY(&rot, 2.094f);

	D3DXVECTOR3 center = D3DXVECTOR3(0,2,0);
	D3DXVECTOR3 n1 = D3DXVECTOR3(0,0,1);
	D3DXVECTOR3 n2;
	D3DXVECTOR3 n3;
	D3DXVec3TransformCoord(&n2, &n1, &rot);
	D3DXVec3TransformCoord(&n3, &n2, &rot);
	
	const int c = _step;
	int side = (int)_side /2;
	int nb = _side/c - 1;
	WORD* i = new WORD[6 *2* nb];
	//Vertex v[4 * nb];
	Vertex* v = new Vertex[4 *2* nb];

	int k = _offset;

	for (int j = 0; j < _side/c-1; j++)
		
	{
		float y = _height[(int)(j*c * _side + k*c)];
		float y2 = _height[(int)((j+1)*c * _side + k*c)];
		float y3 = _height[(int)(j*c * _side + (k+1)*c)];
		setBlade(j*2, D3DXVECTOR3(j*c,y,k*c),
			D3DXVECTOR3((j+1)*c,y2,(k)*c),
			n1, v,i);

		setBlade(j*2+1, D3DXVECTOR3(j*c,y,k*c),
			D3DXVECTOR3(j*c,y3,(k+1)*c),
			D3DXVECTOR3(1,0,0), v,i);
		//setBlade(3*j+1, D3DXVECTOR3(j*c,y,k*c), n2, v,i);
		//setBlade(3*j+2, D3DXVECTOR3(j*c,y,k*c), n3, v,i);
	}

	


	SetVertices(v, 4*2*nb);

	SetIndices(i, 6*2*nb);
	_primType = D3DPT_TRIANGLELIST;
	_nbPrims = 2 *2* nb;
}

void Grass::Draw( Effect *effect, ICamera *cam )
{
	effect->setParameter("rand", _rnd);
	__super::Draw(effect, cam);
	
}
