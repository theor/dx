#include "StdAfx.h"
#include "Entity.h"
#include "ICamera.h"


Entity::Entity(void)
	:  _primType(D3DPT_TRIANGLELIST)
{
	D3DXMatrixTranslation(&_translationMatrix, 0, 0, 0);
	D3DXMatrixRotationYawPitchRoll(&_rotationMatrix, 0, 0, 0);
	_indexes = NULL;
	SetScale(D3DXVECTOR3(1,1,1));
}


Entity::~Entity( void )
{
	if(_vertices != NULL)
		_vertices->Release();
	if(_indexes != NULL)
		_indexes->Release();
}

void Entity::LoadContent( LPDIRECT3DDEVICE9 device )
{
	_device = device;
}

void Entity::Draw( Effect *fx, ICamera *cam )
{
	ID3DXEffect* effect = fx->getEffect();

	_device->SetStreamSource(0, _vertices, 0, sizeof(Vertex));

	if(_primType != D3DPT_TRIANGLEFAN)
		_device->SetIndices(_indexes);
	
	D3DXMATRIX world;
	D3DXMatrixIdentity(&world);
	world *= _scaleMatrix;//*_translationMatrix;
//	D3DXMatrixIdentity(&world);
	D3DXMATRIX WorldViewProj = world;
//	D3DXMatrixIdentity(&world);
	if(cam != NULL)
	{
		//world = _translationMatrix;// * _rotationMatrix;
		WorldViewProj= world * cam->View() * cam->Projection();
		D3DXHANDLE ViewParam = effect->GetParameterByName(NULL, "view");
		effect->SetMatrix(ViewParam , &cam->View());

		D3DXHANDLE ProjParam = effect->GetParameterByName(NULL, "proj");
		effect->SetMatrix(ProjParam , &cam->Projection());
	}
	D3DXHANDLE worldViewProjParam = effect->GetParameterByName(NULL, "worldViewProj");
	effect->SetMatrix(worldViewProjParam , &WorldViewProj);

	D3DXHANDLE worldParam = effect->GetParameterByName(NULL, "world");
	effect->SetMatrix(worldParam , &world);


	unsigned int cPasses, iPass;
	effect->Begin(&cPasses, 0);
	for (iPass= 0; iPass< cPasses; ++iPass)
	{
		effect->BeginPass(iPass);
		effect->CommitChanges();
		if(_primType == D3DPT_TRIANGLEFAN)
			_device->DrawPrimitive(_primType, 0, _nbPrims);
		else
			_device->DrawIndexedPrimitive(_primType, 0, 0, _nbVertices, 0, _nbPrims);
		effect->EndPass();
	}
	effect->End();
}

//void Entity::SetVertices(IDirect3DVertexBuffer9** buffer, Vertex vertices[], int nb)
//{
//	_device->CreateVertexBuffer(nb * sizeof(Vertex), 0, 0, D3DPOOL_MANAGED, &buffer, NULL);
//	VOID* pVoid = NULL;
//	buffer->Lock(0, 0, (void**)&pVoid, 0);
//	memcpy(pVoid, vertices, nb * sizeof(Vertex));
//	buffer->Unlock();
//}

void Entity::SetVertices(Vertex vertices[], int nb)
{
	_device->CreateVertexBuffer(nb * sizeof(Vertex), 0, 0, D3DPOOL_MANAGED, &_vertices, NULL);
	VOID* pVoid = NULL;
	_vertices->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, nb * sizeof(Vertex));
	_vertices->Unlock();

	_nbVertices = nb;

}

void Entity::SetIndices(IDirect3DIndexBuffer9** buffer, WORD indexes[], int nb){
	_device->CreateIndexBuffer(nb * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, buffer, NULL);
	void* pi = NULL;
	HRESULT r = (*buffer)->Lock( 0, nb * sizeof(WORD), (void**)&pi, 0 );
	if(FAILED(r))DebugBreak();

	memcpy( pi, indexes, nb * sizeof(WORD) );
	(*buffer)->Unlock();
}

void Entity::SetIndices(WORD indexes[], int nb){
	_device->CreateIndexBuffer(nb * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &_indexes, NULL);
	void* pi = NULL;
	HRESULT r = _indexes->Lock( 0, nb * sizeof(WORD), (void**)&pi, 0 );
	if(FAILED(r))DebugBreak();

	memcpy( pi, indexes, nb * sizeof(WORD) );
	_indexes->Unlock();

	_nbIndexes = nb;
}

Entity* Entity::SetRotation( D3DXVECTOR3 rotation )
{
	D3DXMatrixRotationYawPitchRoll(&_rotationMatrix, rotation.x, rotation.y, rotation.z);
	return this;
}

Entity* Entity::SetPosition( D3DXVECTOR3 translation )
{
	D3DXMatrixTranslation(&_translationMatrix, translation.x, translation.y, translation.z);
	return this;
}

Entity* Entity::SetScale( D3DXVECTOR3 scale )
{
	D3DXMatrixScaling(&_scaleMatrix, scale.x, scale.y, scale.z);
	return this;
}
