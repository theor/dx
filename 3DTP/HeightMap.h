#pragma once
#include "Entity.h"
#include "ICamera.h"
#include <d3d9.h>
class HeightMap
	: public Entity
{
public:
	HeightMap(void);

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );

	unsigned short getSize(){return m_sizeX;}
	float* getHeight(){return m_height;}
	virtual void Draw( Effect *effect, ICamera *cam );
private:
	unsigned short m_sizeX, m_maxY, m_sizeZ;
	float *m_height;
	bool LoadRAW (const std::string& map);

	IDirect3DTexture9* _texture;
};

