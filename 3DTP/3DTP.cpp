// 3DTP.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "3DTP.h"
#include "Circle.h"
#include <list>
#include "DualTriangle.h"
#include "Inputs.h"
#include "HeightMap.h"
#include "OrthoCamera.h"

#include "Effect.h"
#include "Grass.h"
#include "GrassField.h"

#include "Clouds.h"
#include "Sky.h"

// Global Variables:
HINSTANCE hInst;			// current instance
HWND			hWnd;				// windows handle used in DirectX initialization

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
bool				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);


//DX
LPDIRECT3D9 d3d;
LPDIRECT3DDEVICE9 d3ddev;
IDirect3DVertexDeclaration9* decl;
IDirect3DVertexDeclaration9* declQuad;
Effect* pEffect = NULL;
Effect* furFx = NULL;

//LPDIRECT3DTEXTURE9 pRenderTexture = NULL;
//LPDIRECT3DSURFACE9 pRenderSurface = NULL,pBackBuffer = NULL;
//
//LPDIRECT3DTEXTURE9 pRenderTextureDepth = NULL;
//LPDIRECT3DSURFACE9 pRenderSurfaceDepth = NULL;

//IDirect3DSurface9* surface_ = NULL;
IDirect3DSurface9* origTarget_ = NULL;

IDirect3DTexture9* tex;
IDirect3DTexture9* noise;
IDirect3DSurface9* topSurf;


LPDIRECT3DTEXTURE9 lensflare;


IDirect3DTexture9* normalRt;
IDirect3DSurface9* normalSurface;

IDirect3DTexture9* selected;

ID3DXRenderToSurface* pRTS;

D3DXMATRIX matOldProjection;

Camera *cam = new Camera();
ICamera *ocam = new OrthoCamera();

LPDIRECT3DTEXTURE9 texture;
LPDIRECT3DTEXTURE9 textureGrass;

const char* tech = ("Default");
const char* quadTech = ("Quad");

D3DVIEWPORT9 viewPort;

std::list<IDrawableEntity*> entities;
Quad quad;
GrassField* g;
HeightMap* map;

void loadContent()
{
	for(std::list<IDrawableEntity*>::iterator it = entities.begin();
		it != entities.end();
		it++)
		(*it)->LoadContent(d3ddev);
}


void initDx()
{

	d3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DDISPLAYMODE displayMode;
	D3DPRESENT_PARAMETERS pp;

	ZeroMemory(&pp, sizeof(pp));
	d3d->GetAdapterDisplayMode (D3DADAPTER_DEFAULT, &displayMode);
	pp.Windowed = true;
	pp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	pp.BackBufferWidth = 1024;
	pp.BackBufferHeight = 768;
	pp.hDeviceWindow = hWnd;
	pp.BackBufferFormat = D3DFMT_X8R8G8B8; //displayMode.Format; 
	pp.BackBufferCount = 1;
	pp.MultiSampleType = D3DMULTISAMPLE_NONE ; 
	pp.MultiSampleQuality = 0;
	pp.EnableAutoDepthStencil = TRUE;
	pp.AutoDepthStencilFormat = D3DFMT_D16;
	
	//pp.AutoDepthStencilFormat = D3DFMT_D24S8;
	pp.Flags = 0;
	pp.FullScreen_RefreshRateInHz = 0;
	pp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;/**/

	if(FAILED(d3d->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&pp,
		&d3ddev)))
		DebugBreak();

	// DECLARATIONS

	D3DVERTEXELEMENT9 dwDecl3[] = {
		{0, 0,  D3DDECLTYPE_FLOAT3,  D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12,  D3DDECLTYPE_FLOAT3,  D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2,  D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		D3DDECL_END()
	};
	d3ddev->CreateVertexDeclaration(dwDecl3, &decl);

	D3DVERTEXELEMENT9 dwDecl3quad[] = {
		{0, 0,  D3DDECLTYPE_FLOAT3,  D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT2,  D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		D3DDECL_END()
	};
	d3ddev->CreateVertexDeclaration(dwDecl3quad, &declQuad);


	
	//SHADERS
	Effect::setDevice(&d3ddev);

	pEffect = Effect::get(L"Effects\\effect.fx");

	furFx = Effect::get(L"Effects\\fur.fx");

	
	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\grass.jpg", &texture)))
		DebugBreak();
	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\noise.tga", &noise)))
		DebugBreak();

	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\flare.png", &lensflare)))
		DebugBreak();

	IDirect3DTexture9* furtex;
	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\blade.dds", &furtex)))
		DebugBreak();

	furFx->setParameter("FurTexture", noise);
	furFx->setParameter("Texture", furtex);
	
	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\blade.dds", &textureGrass)))
		DebugBreak();
	//D3DXCreateTextureFromFileEx(d3ddev, L"blade.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &textureGrass);

	LPDIRECT3DTEXTURE9 texDetail;
	if(FAILED(D3DXCreateTextureFromFile(d3ddev, L"Textures\\normal.jpg", &texDetail)))
		DebugBreak();
	pEffect->setParameter("texDetail", texDetail);


	
	HRESULT hr = d3ddev->CreateTexture( 1024, 768, 0, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &tex, NULL);
	if(FAILED(hr))DXTrace(__FILE__, __LINE__, hr, L"YYY", true);
	hr = tex->GetSurfaceLevel( 0 ,  &topSurf);
	if(FAILED(hr))DXTrace(__FILE__, __LINE__, hr, L"YYY", true);
	
	hr = d3ddev->CreateTexture( 1024, 768, 0, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &normalRt, NULL);
	if(FAILED(hr))DXTrace(__FILE__, __LINE__, hr, L"YYY", true);
	hr = normalRt->GetSurfaceLevel( 0 ,  &normalSurface);
	if(FAILED(hr))DXTrace(__FILE__, __LINE__, hr, L"YYY", true);

	selected = tex;

	hr = D3DXCreateRenderToSurface( d3ddev, 1024, 768, D3DFMT_X8R8G8B8,		true, D3DFMT_D24X8, &pRTS);
	if(FAILED(hr))DXTrace(__FILE__, __LINE__, hr, L"YYY", true);
	

	d3ddev->GetRenderTarget( 0, &origTarget_ );

	viewPort.X = 0;
	viewPort.Y = 0;
	viewPort.Width = 1280;
	viewPort.Height = 1024;
	viewPort.MinZ = 0.0f;
	viewPort.MaxZ = 1.0f;


	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); 
	//d3ddev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME); 


	quad.LoadContent(d3ddev);
	//quad./*SetPosition(D3DXVECTOR3(0.5,0.5,0))->*/SetScale(D3DXVECTOR3(0.5f,0.6f,1));
	//Circle *t = new Circle();
	//t->SetPosition(D3DXVECTOR3(3, -2, 0));
	//entities.push_front(t);

	//entities.push_back((new Quad())->SetPosition(D3DXVECTOR3(0,0, 0)));
	/*entities.push_back((new Triangle())->SetPosition(D3DXVECTOR3(-3, -2, 0)));
	entities.push_back((new DualTriangle())->SetPosition(D3DXVECTOR3(3, 2, 0)));*/
	
	Sky *sky = new Sky();
	sky->SetPosition(D3DXVECTOR3(0, -200, 0));
	sky->SetScale(D3DXVECTOR3(5000, 5000, 5000));
	entities.push_back(sky);

	map = new HeightMap();
	map->SetPosition(D3DXVECTOR3(3, 2, 0));
	entities.push_back(map);

	Clouds *c = new Clouds();
	entities.push_back(c);

	loadContent();

	const int step = 2;
	g = new GrassField(map->getSize(), map->getHeight(), step);
	//entities.push_back(g);
	g->LoadContent(d3ddev);


	cam->SetPosition(D3DXVECTOR3(290,55,313));
	cam->SetDirection(D3DXVECTOR3(-0.598,0,-0.8));
	/*for(int i =0; i++; i < map->getSize()/step)
	{*/
		//g = new Grass(map->getSize(), map->getHeight(), 10, 8, D3DXVECTOR3(1,0,0));
		//g->LoadContent(d3ddev);

	//}
}

bool b = true;
long ticks  = 0;

void update()
{
	for(std::list<IDrawableEntity*>::iterator it = entities.begin();
		it != entities.end();
		it++)
	{
		(*it)->Update();
	}
}

void draw() 
{
	ticks++;
	cam->Update();

	if (Inputs::IsPressedOnce(VK_ESCAPE))
		b = !b;

	// Enable alpha blending.
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
	d3ddev->SetRenderState(D3DRS_BLENDOP,D3DBLENDOP_ADD);

	D3DXVECTOR3 pos = cam->Position();
	pEffect->setParameter("camDir", &D3DXVECTOR4(pos.x, pos.y, pos.z, 1));
	pEffect->setParameter("tex", texture);
	pEffect->setParameter("flare", lensflare);
	pEffect->setParameter("time", ticks);

	if(cam != NULL)
	{
		pEffect->setParameter("view", &cam->View());
		pEffect->setParameter("proj", &cam->Projection());
	}

	D3DXVECTOR4 lpos = D3DXVECTOR4(128 + cos((float)ticks / 50.0f) * 50, 20, 128 + sin((float)ticks / 50.0f) * 50, 0);
	pEffect->setParameter("lightPos", &lpos);
	pEffect->setTechnique(tech);

	d3ddev->SetRenderTarget(0, topSurf);
	d3ddev->SetRenderTarget(1, normalSurface);
	d3ddev->BeginScene();
	d3ddev->SetVertexDeclaration(decl);
	//HRESULT hr = pRTS->BeginScene( topSurf, &viewPort);

	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	
	//d3ddev->SetVertexDeclaration(decl);
	for(std::list<IDrawableEntity*>::iterator it = entities.begin();
		it != entities.end();
		it++)
	{
		d3ddev->SetVertexDeclaration(decl);
		(*it)->Draw(pEffect, cam);
	}


	furFx->setParameter("time", ticks);
	/*const int nbLayers = 25;
	for (int i = 0; i < nbLayers; i++)
	{
		furFx->setParameter("CurrentLayer", i / (float)nbLayers);
		map->Draw(furFx, cam);
	}*/
	
	//pEffect->setTechnique("Grass");
	//pEffect->setParameter("tex", textureGrass);
	//g->Draw(pEffect, cam);
	//d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

	d3ddev->EndScene();

	// ************************************************
	d3ddev->SetRenderTarget(0, origTarget_);
	d3ddev->SetRenderTarget(1, NULL);
	if(FAILED(d3ddev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 10), 1.0f, 0)))DebugBreak();
	pEffect->setTechnique("Quad");
	
	D3DXMATRIX ViewProj = cam->View() * cam->Projection();
	pEffect->setParameter("tex", selected);
	pEffect->setParameter("camDir", &D3DXVECTOR4(pos.x, pos.y, pos.z, 1));
	pEffect->setParameter("ViewProj", &ViewProj);	

	d3ddev->BeginScene();
	d3ddev->SetVertexDeclaration(decl);
	quad.Draw(pEffect, ocam);
	d3ddev->EndScene();

	d3ddev->Present(NULL, NULL, NULL, NULL);
}

void clean() 
{
	if(d3ddev != NULL)
		d3ddev->Release();
	if(d3d != NULL)
		d3d->Release();

	for(std::list<IDrawableEntity*>::iterator it = entities.begin();
		it != entities.end();
		it++)
		delete(*it);
}

int APIENTRY WinMain(HINSTANCE	hInstance,
                     HINSTANCE	hPrevInstance,
                     LPSTR			lpCmdLine,
                     int				nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG oMsg;

	// Initialize global strings
	MyRegisterClass(hInstance);


	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
		return false;
	initDx();

	PeekMessage( &oMsg, NULL, 0, 0, PM_REMOVE );
	while ( oMsg.message != WM_QUIT )
	{

		if (PeekMessage( &oMsg, NULL, 0, 0, PM_REMOVE )) 
		{
			TranslateMessage( &oMsg );
			DispatchMessage( &oMsg );
		}
		else
		{
			update();
			draw();
		}
	}

	clean();
	return (int) oMsg.wParam;
}



//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	memset(&wcex, 0, sizeof(WNDCLASSEX));
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style					= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc		= WndProc;
	wcex.hInstance			= hInstance;
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszClassName	= L"3DTPClassName";

	return RegisterClassEx(&wcex);
}

//
//   PURPOSE: Saves instance handle and creates main window
//
bool InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(L"3DTPClassName", L"I like Math and 3D and my 3D teacher", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
      return false;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return true;
}

//
//  PURPOSE:  Processes messages for the main window.
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	Inputs::Get()->Update(message, wParam);
	if(Inputs::IsPressedOnce(VK_NUMPAD0))
		selected = tex;
	if(Inputs::IsPressedOnce(VK_NUMPAD1))
		selected = normalRt;
	
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
