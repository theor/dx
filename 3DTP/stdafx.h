// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>

#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

#ifndef V
#	define V(x)           { hr = (x); if( FAILED(hr) ) { DebugBreak(); } }
#endif

//#define D3D_DEBUG_INFO

struct VertexQuad
{
	D3DXVECTOR3 position;
	//D3DCOLOR color;
	D3DXVECTOR2 uv;
};

struct Vertex
{
	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 uv;
};

// TODO: reference additional headers (like DirectX one) your program requires here
