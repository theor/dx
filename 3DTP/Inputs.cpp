#include "StdAfx.h"
#include "Inputs.h"


#include "StdAfx.h"
#include "Inputs.h"


Inputs::Inputs(void)
{
	for (WPARAM key = 0; key < 0xFE; key++)
	{
		_keys[key] = false;
		_oldKeys[key] = false;
	}
}


Inputs::~Inputs(void)
{
}

bool Inputs::IsPressed( WPARAM keyCode )
{
	return Get()->_keys[keyCode] == true;
}

void Inputs::Update( UINT message, WPARAM keyCode )
{
	switch (message)
	{
	case WM_KEYDOWN:
		Inputs::_keys[keyCode] = true;
		Inputs::_oldKeys[keyCode] = true;
		break;
	case WM_KEYUP:
		Inputs::_keys[keyCode] = false;
		Inputs::_oldKeys[keyCode] = false;
		break;
	}
}

Inputs* Inputs::Get()
{
	static Inputs *_instance;
	if(_instance == NULL)
		_instance = new Inputs();
	return _instance;
}

bool Inputs::IsPressedOnce( WPARAM keyCode )
{
	bool b= Get()->_oldKeys[keyCode];
	Get()->_oldKeys[keyCode] = false;
	return b;
}
