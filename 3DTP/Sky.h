#pragma once

#include <sstream>

#include "Entity.h"
#include "IDrawableEntity.h"
#include "Camera.h"
#include "Effect.h"

class Sky : public IDrawableEntity, public IEntity
{
public:
	Sky();
	~Sky();

	Sky* SetPosition(D3DXVECTOR3 translation);
	Sky* SetScale(D3DXVECTOR3 scale);
	D3DXMATRIX GetPosition(){return _translationMatrix;}

	virtual void Draw( Effect *effect, ICamera *cam );
	virtual void LoadContent( LPDIRECT3DDEVICE9 device );
	virtual void Update();

private:
	D3DXMATRIX _translationMatrix;
	D3DXMATRIX _rotationMatrix;
	D3DXMATRIX _scaleMatrix;
	LPDIRECT3DDEVICE9 _device;

	std::string _filename;
	LPD3DXMESH _pMesh;
	D3DMATERIAL9* _pMeshMaterials;
	LPDIRECT3DTEXTURE9* _pMeshTextures;
	DWORD _dwNumMaterials;

	IDirect3DTexture9* _skyTexture;
	IDirect3DTexture9* _skyTexture2;

	D3DXVECTOR4 _offset;
};

