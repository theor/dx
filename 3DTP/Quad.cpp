#include "StdAfx.h"
#include "Quad.h"

Quad::Quad(void)
{
}

void Quad::LoadContent( LPDIRECT3DDEVICE9 device )
{
	__super::LoadContent(device);

	Vertex v[] = {
		{D3DXVECTOR3(-1, 1,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0)},
		{D3DXVECTOR3( 1, 1,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0)},
		{D3DXVECTOR3(-1,-1,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1)},
		{D3DXVECTOR3( 1,-1,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1)},
	};

	SetVertices(v, 4);

	WORD i[] ={	0, 1, 2, 3};
	SetIndices(i, sizeof(i));
	_primType = D3DPT_TRIANGLESTRIP;
	_nbPrims = 2;
}

void Quad::LoadContent( LPDIRECT3DDEVICE9 device, float offsetX = 0, float offsetY = 0, float width = 2, float height = 2)
{
	__super::LoadContent(device);

	float l = -1 + offsetX;
	float r = l + width;
	float t = -1 + offsetY;
	float b = l + height;

	Vertex v[] = {
		{D3DXVECTOR3(l,b,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0)},
		{D3DXVECTOR3(r,b,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0)},
		{D3DXVECTOR3(l,t,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1)},
		{D3DXVECTOR3(r,t,0.5), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1)},
	};

	SetVertices(v, 4);

	WORD i[] ={	0, 1, 2, 3};
	SetIndices(i, sizeof(i));
	_primType = D3DPT_TRIANGLESTRIP;
	_nbPrims = 2;
}

void Quad::Draw( Effect *effect, ICamera *cam )
{
	__super::Draw(effect, cam);
}
