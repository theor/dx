#include "StdAfx.h"
#include "Cloud.h"


Cloud::Cloud(float lifeTime, D3DXVECTOR3 position, D3DXVECTOR3 velocity)
{
	_alpha = 0;
	_startLifeTime = lifeTime;
	_lifeTime = lifeTime;
	_velocity = velocity;
	SetPosition(position);

	_vertices = NULL;
	_indexes = NULL;
}

Cloud::~Cloud(void)
{
}

bool
Cloud::IsAlive() const
{
	return _lifeTime > 0;
}

void Cloud::LoadContent(LPDIRECT3DDEVICE9 device)
{
	_device = device;

	int random = rand() % 4;
	LPCWSTR str = L"Textures\\cloud_1.png";
	if (random == 1)
		str = L"Textures\\cloud_2.png";
	else if  (random == 2)
		str = L"Textures\\cloud_3.png";
	else if  (random == 3)
		str = L"Textures\\cloud_4.png";

	_scale = 50 + rand() % 100;

	if(FAILED(D3DXCreateTextureFromFile(device, str, &_cloudsTexture)))
		DebugBreak();
	
	Vertex v[] = {
		{D3DXVECTOR3(0,0,0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0)},
		{D3DXVECTOR3(0,0,0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0)},
		{D3DXVECTOR3(0,0,0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1)},
		{D3DXVECTOR3(0,0,0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1)},
	};

	SetVertices(v, 4);

	WORD i[] = {0, 1, 2, 3};
	SetIndices(i, sizeof(i));
	_nbPrims = 2;
}

void Cloud::SetVertices(Vertex vertices[], int nb)
{
	if (_vertices == NULL)
		_device->CreateVertexBuffer(nb * sizeof(Vertex), 0, 0, D3DPOOL_MANAGED, &_vertices, NULL);
	
	VOID* pVoid = NULL;
	_vertices->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, nb * sizeof(Vertex));
	_vertices->Unlock();

	_nbVertices = nb;
}

void Cloud::SetIndices(WORD indexes[], int nb)
{
	if (_indexes == NULL)
		_device->CreateIndexBuffer(nb * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &_indexes, NULL);
	
	void* pi = NULL;
	HRESULT r = _indexes->Lock( 0, nb * sizeof(WORD), (void**)&pi, 0 );
	if(FAILED(r))DebugBreak();

	memcpy( pi, indexes, nb * sizeof(WORD) );
	_indexes->Unlock();

	_nbIndexes = nb;
}

void Cloud::Update()
{
	_lifeTime -= 0.1;

	// Born
	if (_lifeTime > 4 * _startLifeTime / 5)
	{
		_alpha += 0.002f;
	}

	// Death
	else if (_lifeTime < _startLifeTime / 5)
	{
		_alpha -= 0.002f;
	}

	// Life
	_position += _velocity;
}

void Cloud::Draw(Effect* effect, ICamera *cam)
{
	D3DXMATRIX mat = cam->View();

	D3DXVECTOR3 rightVect = D3DXVECTOR3(mat._11, mat._21, mat._31) * _scale;
	D3DXVECTOR3 upVect = D3DXVECTOR3(mat._12,mat._22,mat._32) * _scale;

	Vertex v[] = {
		{_position + upVect, D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0)},
		{_position + rightVect, D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0)},
		{_position - rightVect, D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1)},
		{_position - upVect, D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1)},
	};

	SetVertices(v, 4);

	_device->SetStreamSource(0, _vertices, 0, sizeof(Vertex));
	_device->SetIndices(_indexes);
	
	effect->setTechnique("Clouds");
	effect->setParameter("tex", _cloudsTexture);

	D3DXMATRIX world;
	D3DXMatrixIdentity(&world);

	D3DXMATRIX WorldViewProj = world;

	if(cam != NULL)
	{
		WorldViewProj = world * cam->View() * cam->Projection();
		
		effect->setParameter("view", &cam->View());
		effect->setParameter("proj", &cam->Projection());
	}
	effect->setParameter("world", &world);
	effect->setParameter("worldViewProj", &WorldViewProj);
	effect->setParameter("cloudAlpha", _alpha < 0 ? 0 : _alpha > 1 ? 1 : _alpha);

	ID3DXEffect *e = effect->getEffect();

	unsigned int cPasses, iPass;
	e->Begin(&cPasses, 0);
	for (iPass= 0; iPass< cPasses; ++iPass)
	{
		e->BeginPass(iPass);
		e->CommitChanges();
		_device->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, _nbVertices, 0, _nbPrims);
		e->EndPass();
	}
	e->End();

	effect->setTechnique("Default");
}

Cloud* Cloud::SetRotation(float rotation)
{
	_rotation = rotation;
	return this;
}

Cloud* Cloud::SetPosition( D3DXVECTOR3 translation )
{
	_position = translation;
	return this;
}

Cloud* Cloud::SetScale( D3DXVECTOR3 scale )
{
	D3DXMatrixScaling(&_scaleMatrix, scale.x, scale.y, scale.z);
	return this;
}