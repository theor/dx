#include "StdAfx.h"
#include "Camera.h"
#include "Inputs.h"


Camera::Camera(void)
{
	D3DXMatrixPerspectiveFovLH(&_projection,
		D3DXToRadian(45),    // the horizontal field of view
		(FLOAT)1024.0f / (FLOAT)768.0f,    // aspect ratio
		0.2f,    // the near view-plane
		10000.0f);    // the far view-plane

	_position = D3DXVECTOR3(128.0f, 25.0f, 0);
	SetDirection(D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	_up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}


Camera::~Camera(void)
{
}

void Camera::LoadContent( LPDIRECT3DDEVICE9 device )
{
}

void Camera::Update()
{
	float f = 0.05f;

	
	D3DXVECTOR3 side;
	D3DXVec3Cross(&side, &_lookDir, &D3DXVECTOR3(0, 1, 0));
	D3DXVec3Normalize(&side, &side);

	if(Inputs::IsPressed(VK_CONTROL))
	{
		if(Inputs::IsPressed(VK_LEFT))
			_position.y	+= 5;
		if(Inputs::IsPressed(VK_RIGHT))
			_position.y	-= 5;
	}
	else
	{
		if(Inputs::IsPressed(VK_LEFT))
			_position += side;
		if(Inputs::IsPressed(VK_RIGHT))
			_position -= side;
	}

	D3DXMATRIX rot;
	float angle = 0;
	if(Inputs::IsPressed(0x57))
		angle += f;
	if(Inputs::IsPressed(0x53))
		angle -= f;
	D3DXMatrixRotationAxis(&rot, &side, angle);
	D3DXVec3TransformCoord(&_lookDir, &_lookDir, &rot);

	angle = 0;
	if(Inputs::IsPressed(0x41))
		angle -= f;
	if(Inputs::IsPressed(0x44))
		angle += f;
	D3DXMatrixRotationY(&rot, angle);
	D3DXVec3TransformCoord(&_lookDir, &_lookDir, &rot);


	if(Inputs::IsPressed(VK_UP))
		_position += _lookDir;
	if(Inputs::IsPressed(VK_DOWN))
		_position -= _lookDir;


	_lookat = _position + _lookDir;
	D3DXMatrixLookAtLH(&_view, &_position, &_lookat, &_up);
}
