#pragma once
#include "Entity.h"
class DualTriangle
	: public Entity
{
public:
	DualTriangle(void);
	virtual ~DualTriangle(void);

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );

	virtual void Draw(ID3DXEffect *effect, ICamera *cam);
private:
	IDirect3DIndexBuffer9*  _indexes2;
};

