#pragma once
#include <map>
#include "stdafx.h"
#include <d3d9.h>

class Effect
{
public:
	typedef std::map<std::wstring, Effect*> EffectMap;
	typedef std::map<std::string, D3DXHANDLE> HandleMap;

	static Effect* get(LPCWSTR effect);
	static LPD3DXEFFECT getRaw(LPCWSTR effect);
	static void setDevice(LPDIRECT3DDEVICE9* device){_device = device;};
private:
	static EffectMap _effectsPool;
	static LPDIRECT3DDEVICE9* _device;
public:
	Effect(LPDIRECT3DDEVICE9* device, LPCWSTR effect);
	~Effect(void);
	LPD3DXEFFECT getEffect(){return _effect;}

	void setParameter(const char * param, D3DXMATRIX* v);
	void setParameter(const char * param, D3DXVECTOR4* v);
	void setParameter(const char * param, LPDIRECT3DTEXTURE9 v);
	void setParameter(const char * param, float v);
	void setTechnique(const char* technique);


private:
	LPCWSTR _effectName;
	LPD3DXEFFECT _effect;

	HandleMap _techniquesPool;
	HandleMap _paramsPool;

	D3DXHANDLE getOrCreateHandle(const char* name);
};

