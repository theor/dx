#pragma once
#include "Entity.h"

class Grass
	: public Entity
{
public:
	Grass(float side, float* height, int offset, int step, D3DXVECTOR3 n);
	~Grass(void);

	virtual void LoadContent( LPDIRECT3DDEVICE9 device );

	virtual void Draw( Effect *effect, ICamera *cam );

private:
	float _side;
	float* _height;
	int _offset;
	int _step;
	D3DXVECTOR3 _normal;
	float _rnd;
};

