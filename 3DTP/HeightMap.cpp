#include "StdAfx.h"
#include "HeightMap.h"
#include <stdio.h>


HeightMap::HeightMap(void)
	: m_maxY(20)
{
}


//HeightMap::~HeightMap(void)
//{
//}

bool HeightMap::LoadRAW (const std::string& map)
{
	FILE *file;

	fopen_s(&file, map.c_str (), "rb");
	if (!file)
		return false;

	fread(&m_sizeX, sizeof(unsigned short), 1, file);
	fread(&m_sizeZ, sizeof(unsigned short), 1, file);

	unsigned int size = m_sizeX * m_sizeZ;
	unsigned char *tmp = new unsigned char[size];
	m_height = new float[size];

	fread(tmp, sizeof(unsigned char), size, file);

	fclose(file);

	int i = 0;

	for (unsigned short z = 0; z < m_sizeZ; ++z)
		for (unsigned short x = 0; x < m_sizeX; ++x, ++i)
			m_height[i] = float ((m_maxY * tmp[i]) / 255.0f);

	delete[] tmp;
	return true;

}



void HeightMap::LoadContent( LPDIRECT3DDEVICE9 device )
{
	__super::LoadContent(device);

	if(FAILED(D3DXCreateTextureFromFile(device, L"Textures\\grass.jpg", &_texture)))
		DebugBreak();

	LoadRAW("Textures\\terrainheight.raw");

	int side = m_sizeX;

	Vertex *v = new Vertex[side * side];
	WORD *ind = new WORD[side * side * 6];

	int ii = 0;

	for (int j = 0; j < side; j++)
	{
		for (int i = 0; i < side; i++)
		{
		int h = i*m_sizeX + j;//(i * side / m_sizeZ) * m_sizeX + (j* side / m_sizeX);

		Vertex *vx = &v[i * side + j];
		vx->uv.x = i / (float)side;
		vx->uv.y = j / (float)side;

		const D3DXVECTOR3& pos = D3DXVECTOR3(i * 5, m_height[h] * 5, j * 5);
		vx->position = pos;
		vx->normal = D3DXVECTOR3(0,0,0);

			if(i < side -1 && j < side-1)
			{
				ind[ii++] = j * side + i;
				ind[ii++] = j * side + i + 1;
				ind[ii++] = j * side + i + side;
				ind[ii++] = j * side + i + 1;
				ind[ii++] = j * side + i + side + 1;
				ind[ii++] = j * side + i + side;
			}
		}
	}

	unsigned int i = 0;
	D3DXVECTOR3 vec;
	for (unsigned short z = 0; z < side; ++z)
	{
		for (unsigned short x = 0; x < side; ++x, ++i)
		{
			const D3DXVECTOR3& pos = v[i].position;
			v[i].normal.x = 0.0f;
			v[i].normal.y = 0.0f;
			v[i].normal.z = 0.0f;
			D3DXVECTOR3 normal;
			float diffHeight;
			if (z > 0)
			{
				if (z + 1 < m_sizeZ)
					diffHeight = m_height[i - side] - m_height[i + side];
				else
					diffHeight = m_height[i - side] - m_height[i];
			}
			else
				diffHeight = m_height[i] - m_height[i + side];

			vec = D3DXVECTOR3(0.0f, 1.0f, diffHeight);
			D3DXVec3Normalize(&vec, &vec);
			v[i].normal += vec;
			if (x > 0)
			{
				if (x + 1 < side)
					diffHeight = m_height[i - 1] - m_height[i + 1];
				else
					diffHeight = m_height[i - 1] - m_height[i];
			}
			else
				diffHeight = m_height[i] - m_height[i + 1];
			
			vec = D3DXVECTOR3(diffHeight, 1.0f, 0.0f);
			D3DXVec3Normalize(&vec, &vec);

			v[i].normal += vec;
			//v[i].normal = D3DXVECTOR3(0,1,0);

			D3DXVec3Normalize(&v[i].normal, &v[i].normal);
		}
	}
	SetVertices(v, side*side);
	_nbPrims = (side - 1) * (side - 1) * 2;
	SetIndices(ind, _nbPrims*3);
}

void HeightMap::Draw( Effect *effect, ICamera *cam )
{
	effect->setParameter("tex", _texture);
	__super::Draw(effect, cam);
	
}
