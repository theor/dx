#pragma once
#include "stdafx.h"
#include "ICamera.h"
#include "IDrawableEntity.h"
#include "IEntity.h"
#include "Effect.h"

class Entity
	: public IDrawableEntity
{
public:
	Entity(void);
	virtual ~Entity(void);

	virtual void LoadContent(LPDIRECT3DDEVICE9 device);
	virtual void Update(){};
	virtual void Draw(Effect *effect, ICamera *cam);

	Entity* SetPosition(D3DXVECTOR3 translation);
	Entity* SetRotation(D3DXVECTOR3 rotation);
	Entity* SetScale(D3DXVECTOR3 scale);
	D3DXMATRIX GetPosition(){return _translationMatrix;}
protected:
	void SetIndices(IDirect3DIndexBuffer9** buffer, WORD indexes[], int nb);

	virtual void SetVertices(Vertex vertices[], int nb);
	virtual void SetIndices(WORD indexes[], int nb);

	IDirect3DVertexBuffer9*  _vertices;
	IDirect3DIndexBuffer9*  _indexes;
	LPDIRECT3DDEVICE9 _device;
	D3DPRIMITIVETYPE _primType;

	int _nbVertices;
	int _nbIndexes;
	int _nbPrims;

	D3DXMATRIX _translationMatrix;
	D3DXMATRIX _rotationMatrix;
	D3DXMATRIX _scaleMatrix;
};

