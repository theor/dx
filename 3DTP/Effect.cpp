#include "StdAfx.h"
#include "Effect.h"


LPDIRECT3DDEVICE9* Effect::_device;
Effect::EffectMap Effect::_effectsPool;

Effect::Effect( LPDIRECT3DDEVICE9* device, LPCWSTR effect )
	: _effectName(effect)
{
	LPD3DXBUFFER ppCompilationErrors;
	if(D3D_OK != D3DXCreateEffectFromFile(*device,  effect, NULL, NULL, 0, NULL, &_effect, &ppCompilationErrors))
	{
		MessageBoxA (NULL, (char *)
			ppCompilationErrors->GetBufferPointer(), "Error", 0);
		PostQuitMessage(0);
		return;
	}
}

Effect::~Effect(void)
{
	delete(_effect);
}

Effect* Effect::get( LPCWSTR effect )
{
	std::wstring sfx = std::wstring(effect);
	EffectMap::iterator ifx = _effectsPool.find(sfx);
	if(ifx != _effectsPool.end())
		return ifx->second;
	Effect* fx = new Effect(_device, effect);
	_effectsPool.insert(std::pair<std::wstring, Effect*>(sfx, fx));
	return fx;
}

void Effect::setTechnique( const char* technique )
{
	D3DXHANDLE tech = NULL;

	std::string sfx = std::string(technique);
	HandleMap::iterator ifx = _techniquesPool.find(sfx);
	if(ifx != _techniquesPool.end())
		tech = (ifx->second);
	else
	{
		tech = _effect->GetTechniqueByName(technique);
			_techniquesPool.insert(std::pair<std::string, D3DXHANDLE>(sfx, tech));
	}

	_effect->SetTechnique(tech);
}

LPD3DXEFFECT Effect::getRaw( LPCWSTR effect )
{
	return Effect::get(effect)->getEffect();
}

void Effect::setParameter(const char * param, D3DXMATRIX* v)
{
	D3DXHANDLE p = _effect->GetParameterByName(NULL, param);
	_effect->SetMatrix(p, v);
}

void Effect::setParameter( const char * param, D3DXVECTOR4* v )
{
	D3DXHANDLE p = _effect->GetParameterByName(NULL, param);
	_effect->SetVector(p, v);
}

void Effect::setParameter( const char * param, LPDIRECT3DTEXTURE9 v )
{
	D3DXHANDLE p = _effect->GetParameterByName(NULL, param);
	_effect->SetTexture(p,v);
}

void Effect::setParameter( const char * param, float v )
{
	D3DXHANDLE p = _effect->GetParameterByName(NULL, param);
	_effect->SetFloat(p,v);
}

D3DXHANDLE Effect::getOrCreateHandle(const char* name )
{
	D3DXHANDLE param = NULL;

	std::string sfx = std::string(name);
	HandleMap::iterator ifx = _paramsPool.find(sfx);
	if(ifx != _techniquesPool.end())
		param = (ifx->second);
	else
	{
		param = _effect->GetTechniqueByName(name);
		_paramsPool.insert(std::pair<std::string, D3DXHANDLE>(sfx, param));

	}
	return param;
}
