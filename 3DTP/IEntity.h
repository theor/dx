#pragma once
class IEntity
{
public:
	virtual ~IEntity();
	virtual void LoadContent(LPDIRECT3DDEVICE9 device) = 0;
	virtual void Update() = 0;
};

