float4x4 worldViewProj;
float4x4 world;
float4x4 view;
float4x4 proj;
float4x4 ViewProj;

float2 skyMoveOffset;
float3 lightDir = float3(1, -1, 0);
float4 dirLightColor = float4(1,1,1,1);

float4 topColor = float4(12.0f / 255, 137.0f / 255, 236.0f / 255, 1);
float4 bottomColor = float4(181.0f / 255, 222.0f / 255, 255.0f / 255, 1);

float3 lightPos = float3(128, 20, 128);
float4 lightColor = float4(1,1,0.9,1);
float lightRadius = 100;
float3 camDir;

float cloudAlpha;

// Lens flare
float SunSize = 50;
float Density = 3;
float Exposure = -.1;
float Weight = .125;
float Weight2 = .025;
float Decay = .5;
float4 sunPosition = float4(-100000, 200000, -100000, 0);
float lightIntensity = 1.0f;
float3 Color = float3(1,1,1);

texture tex;
sampler Sampler = sampler_state
{
	Texture = <tex>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

texture tex2;
sampler Sampler2 = sampler_state
{
	Texture = <tex2>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

texture flare;
sampler Flare = sampler_state
{
    Texture = (flare);
    AddressU = CLAMP;
    AddressV = CLAMP;
};

texture texDetail;
sampler DetailSampler = sampler_state
{
	Texture = <texDetail>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct VS_OUTPUT
{
    float4 Pos : POSITION;
    float3 Normal : NORMAL;
	float2 texcoord : TEXCOORD0;
	float4 WPos : TEXCOORD1;
	float Depth : TEXCOORD2;
};

struct pso
{
    float4 c0 : COLOR0;
	float4 c1 : COLOR1;
};

///////////////////////////////////////
// Clouds
///////////////////////////////////////
VS_OUTPUT VS_Clouds ( float4 Pos : POSITION,
					  float3 Normal : NORMAL,
					  float2 uv : TEXCOORD )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4 hPos = Pos;
	
	Out.Pos = mul( hPos, worldViewProj);
	Out.Normal = 0.5 * (Normal + 1);
	Out.texcoord = uv;
	Out.WPos = Pos;
	Out.Depth = Out.Pos.z / Out.Pos.w;

	return Out;
}

float4 PS_Clouds(VS_OUTPUT v) : COLOR
{
	float4 color = tex2D(Sampler, v.texcoord);
	color.a *= cloudAlpha;

	return color;
}

///////////////////////////////////////
// Sky
///////////////////////////////////////
VS_OUTPUT VS_Sky ( float4 Pos : POSITION,
				   float3 Normal : NORMAL,
				   float2 uv : TEXCOORD )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4 hPos = Pos;
	
	Out.Pos = mul( hPos, worldViewProj);
	Out.Normal = 0.5 * (Normal + 1);
	Out.texcoord = uv;
	Out.WPos = Pos;
	Out.Depth = Out.Pos.z / Out.Pos.w;

	return Out;
}

float4 PS_Sky(VS_OUTPUT v) : COLOR
{
	float4 baseColor = lerp(bottomColor, topColor, saturate((v.WPos.y)/0.4f));
    float4 cloudValue = tex2D(Sampler, v.texcoord + skyMoveOffset).r + 
						tex2D(Sampler2, v.texcoord + skyMoveOffset / 3).r;
    
    return lerp(baseColor, 1, cloudValue);
}

///////////////////////////////////////
// Default
///////////////////////////////////////
VS_OUTPUT VS( float4 Pos : POSITION,
			  float3 Normal : NORMAL,
			  float2 uv : TEXCOORD )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4 hPos = Pos;
	
	Out.Pos = mul( hPos, worldViewProj);
	Out.Normal = 0.5 * (Normal + 1);
	Out.texcoord = uv * 10;
	Out.WPos = Pos;
	Out.Depth = Out.Pos.z / Out.Pos.w;

	return Out;
}

float3 computeDirLight(VS_OUTPUT v, float3 lightDir, float4 color, float intensity, float3 norm)
{
	float diffuse = max(0,dot(-norm,normalize(lightDir)));
	return diffuse * color * intensity;
}

float3 computePointLight(VS_OUTPUT v, float3 lightPos, float lightRadius, float4 color, float intensity, float3 norm)
{

	float3 lightVector = lightPos - v.WPos;

    float attenuation = saturate(1.0f - length(lightVector)/lightRadius); 
    lightVector = normalize(lightVector); 

	float diffuse = max(0,dot( norm,normalize(lightVector)));

	 float3 reflectionVector = normalize(reflect(-lightVector, norm));
    float3 directionToCamera = normalize(camDir - v.WPos);
    float specular = 4 * pow( saturate(dot(reflectionVector, directionToCamera)), 128);
	
	return (attenuation * (diffuse+ specular))* color   * intensity ;
}

pso PS(VS_OUTPUT v)
{
	pso o;


	float4 t = tex2D(Sampler, v.texcoord);
	v.texcoord *= 10;
	v.texcoord.y = 1 - v.texcoord.y;
	float4 d = tex2D(DetailSampler, v.texcoord);
	
	float4 position =v.WPos;
	float3 lightVector = lightPos - position;

	
	float3 normalFromMap = tex2D(DetailSampler, v.texcoord);
	float3 norm = normalize(v.Normal * (2.0f * normalFromMap - 1.0f));


	float3 dirLight = computeDirLight(v, lightDir, dirLightColor, 1, norm);
	float3 pointLight = computePointLight(v, lightPos, lightRadius, lightColor, 8, norm);

	o.c0 =float4((0.3 + 0.7*saturate(pointLight + dirLight)), 1);
	//o.c0.a = 1;
	o.c0 *=  t;
	//o.c0 = float4(v.Normal, 1);
	o.c1 = float4(v.Normal, v.Depth);

	return o;
}

///////////////////////////////////////
// Quad
///////////////////////////////////////
VS_OUTPUT VS_Quad( float3 Pos : POSITION, float3 Normal : NORMAL, float2 uv : TEXCOORD0 )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4 hPos = float4( Pos, 1);
	Out.Pos = hPos;
	//Out.Pos = mul( hPos, worldViewProj);
	Out.WPos = hPos;
	Out.texcoord = uv;

	return Out;
}

float4 DoLenseFlare(float4 ScreenLightPosition,float2 texCoord,bool fwd)
{
	// Calculate vector from pixel to light source in screen space.  
	float2 deltaTexCoord = (texCoord - ScreenLightPosition.xy);  
	
	// Divide by number of samples and scale by control factor.  
	deltaTexCoord *= 1.0f / 3 * Density;  
	
	// Store initial sample.  
	float3 color = 0;
	
	// Set up illumination decay factor.  
	float illuminationDecay = 1.0f;  
		
	for (int i = 0; i < 3 ; i++)  
	{  	
		// Step sample location along ray.  
		if(fwd)
			texCoord -= deltaTexCoord;  
		else
			texCoord += deltaTexCoord;  
		// Retrieve sample at new location.  
		float3 sample = tex2D(Flare, texCoord);
		
		// Apply sample attenuation scale/decay factors.  
		if(fwd)
			sample *= illuminationDecay * Weight;  
		else
			sample *= illuminationDecay * Weight2;			
		
		// Accumulate combined color.  
		color += sample;  
		// Update exponential decay factor.  
		illuminationDecay *= Decay;
	}  

	return float4(color,1);
}

float4 PS_Quad(VS_OUTPUT input) : COLOR
{
/*
	float4 t =tex2D(Sampler, v.texcoord);
	//t.r += 0.1;
	t.a = 1;
	//t = v.WPos;
	//return float4(v.texcoord.x, v.texcoord.y, 0.1, 1);
	return t;
	*/

	// Get the scene
	float4 col = tex2D(Sampler, input.texcoord);
	
	// Find the suns position in the world and map it to the screen space.
	float4 ScreenPosition = mul(sunPosition - camDir, ViewProj);
	float scale = ScreenPosition.z;

	ScreenPosition.xyz /= ScreenPosition.w;
	ScreenPosition.x = ScreenPosition.x/2.0f+0.5f;
	ScreenPosition.y = (-ScreenPosition.y/2.0f+0.5f);
	
	// get the depth from the depth map
	//float depthVal = 1- tex2D(depthSampler, input.texCoord).r;	 
	
	// Are we lokoing in the direction of the sun?
	if(ScreenPosition.w > 0)
	{		
		float2 coord;		
		float size = SunSize / scale;					
		float2 center = ScreenPosition.xy;

		coord = .5 - (input.texcoord - center) / size * .5;		
		col += (pow(tex2D(Flare, coord) * float4(Color,1),2) * lightIntensity) * 2;				
		
		// Lens flare
		col += ((DoLenseFlare(ScreenPosition, input.texcoord,true) + 
				 DoLenseFlare(ScreenPosition,input.texcoord,false)) * float4(Color,1) * lightIntensity) * 5;
	}
	
	return col;
}

float4 PS_Depth(VS_OUTPUT v) : COLOR
{
	//float4 t = tex2D(Sampler, v.texcoord);
	//t = 1;
	return float4(v.Normal, 1);
}

technique Default
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}

technique Sky
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS_Sky();
		PixelShader = compile ps_3_0 PS_Sky();
	}
}

technique Clouds
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS_Clouds();
		PixelShader = compile ps_3_0 PS_Clouds();
	}
}

technique Quad
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS_Quad();
		PixelShader = compile ps_3_0 PS_Quad();
	}
}

technique Depth
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Depth();
	}
}

float time;
float rand;
VS_OUTPUT VS_Grass( float3 Pos : POSITION, float3 Normal : NORMAL, float2 uv : TEXCOORD0 )
{
	//VS_OUTPUT Out = (VS_OUTPUT)0;
//
	//float4 hPos = float4( Pos, 1);
//
	//if(uv.y < 0.1)
	//hPos.z += 2 * cos(time / 50);
//
	//Out.Pos = hPos;
	//Out.Pos = mul( hPos, worldViewProj);
	//Out.WPos = hPos;
	//Out.texcoord = uv;
	//Out.Normal = Normal;
	//return Out;


	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4 hPos = float4( Pos, 1);
	if(uv.y < 0.1)
	{
		uv.y = 0.1;
		hPos.z += cos(rand*3 + time / 50);
	}
	Out.Pos = mul( hPos, worldViewProj);
	Out.Normal = Normal;//0.5 * (Normal + 1);
	Out.texcoord = uv;
	Out.WPos = hPos;
	Out.Depth = Out.Pos.z / Out.Pos.w;

	return Out;
}


pso PS_Grass(VS_OUTPUT v) : COLOR
{
	float4 t =tex2D(Sampler, v.texcoord);
	
	//t.r += 0.1;
	//if(v.texcoord.y < 0.1)
		//t.a = 0;
	//t = v.WPos;
	//return float4(v.texcoord.x, v.texcoord.y, 0.1, 1);
	//if(t.r <0.1 && t.g < 0.1 && t.b < 0.1)
		//t = float4(1,0,0,0);
//
		
	float4 position =v.WPos;
	float3 lightVector = lightPos - position;

	float4 dirLight = float4(0.5,0.5,0.5,1);//computeDirLight(v, lightDir, dirLightColor, 1);
	
	//

    float attenuation = saturate(1.0f - length(lightVector)/lightRadius); 

	float4 pointLight = attenuation * lightColor;
	pointLight.a = 1;


	pso o;
	//o.c0 = t;
	if(t.a > 0.5)
		t.a = 1;
	else
		t.a = 0;

	o.c0 = t;//* (pointLight + dirLight);
	o.c1 = float4(v.Normal, 1);
	return o;
}

technique Grass
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS_Grass();
		PixelShader = compile ps_3_0 PS_Grass();
	}
}