float4x4 world;
float4x4 view;
float4x4 proj;
 
float CurrentLayer; //value between 0 and 1
float MaxHairLength = 2; //maximum hair length
 
 float time;

texture FurTexture;
sampler FurSampler = sampler_state
{
	Texture = (FurTexture);
	MinFilter = Point;
	MagFilter = Point;
	MipFilter = Point;
	AddressU = Wrap;
	AddressV = Wrap;
};

texture Texture;
sampler Sampler = sampler_state
{
	Texture = (Texture);
	MinFilter = Point;
	MagFilter = Point;
	MipFilter = Point;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VertexShaderInput
{
	float3 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};
 
struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : TEXCOORD1;
};

VertexShaderOutput FurVertexShader(VertexShaderInput input)
{
	VertexShaderOutput output;
	float3 pos;
	pos = input.Position + float3(0,1,0)/*input.Normal*/ * MaxHairLength * (CurrentLayer);
	pos += CurrentLayer * (cos(time / 10 + input.TexCoord.x * 10) + sin(time/17+ input.TexCoord.y*10)) * float3(0.4,0,0);
 
	float4 worldPosition = mul(float4(pos,1), world);
	float4 viewPosition = mul(worldPosition, view);
	output.Position = mul(viewPosition, proj);
	output.Normal = input.Normal;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 FurPixelShader(VertexShaderOutput input) : COLOR0
{
	float4 furColor = tex2D(FurSampler, input.TexCoord * 30);
	float2 coords = float2(input.TexCoord.x + furColor.r + furColor.g + furColor.b, 1-CurrentLayer);

	float4 tex = tex2D(Sampler, coords);

	furColor.a = 1-furColor.r;
	furColor.rgb = tex.rgb;
	float shadow = lerp(0.1,1,CurrentLayer);
	furColor.rgb *= shadow * dot(-input.Normal, float3(0,-1,0));
	return furColor;
}
 
technique Fur
{
	pass Pass1
	{
	AlphaBlendEnable = true;
	SrcBlend = SRCALPHA;
	DestBlend = INVSRCALPHA;
	CullMode = None;
 
	VertexShader = compile vs_2_0 FurVertexShader();
	PixelShader = compile ps_2_0 FurPixelShader();
	}
}